<?php

/**
 * @file
 * Contains the code to generate the custom drush commands for system_monitor.
 */

use Drupal\system_monitor\SystemMonitorLogLevel;

/**
 * Implements hook_drush_command().
 */
function system_monitor_test_drush_command() {
  $items = [];
  $items['smt-gen-logs'] = [
    'description' => 'Generate test logs',
    'aliases' => ['smt:gl'],
  ];
  return $items;
}

/**
 * Generate demo log values for system monitor to test the status page.
 *
 * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
 * @throws \Drupal\Component\Plugin\Exception\PluginException
 * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
 */
function drush_system_monitor_test_smt_gen_logs() {

  $system_monitor_utility = \Drupal::service('system_monitor.utility');
  $system_monitor_plugin_manager = \Drupal::service('plugin.manager.system_monitor_task');
  $system_monitor_logger = \Drupal::service('system_monitor_logger');

  $active_monitors = $system_monitor_utility->getActiveMonitorIds();
  $log_levels = $system_monitor_logger->getRfcToSystemMonitorMap();

  foreach ($active_monitors as $active_monitor) {
    $tasks = $system_monitor_plugin_manager->getAvailableTasksByMonitorId($active_monitor, [], TRUE);
    foreach ($tasks as $task => $task_label) {
      $num_log = 50;
      $logs = [];
      for ($i = 0; $i < $num_log; $i++) {
        $days_ago = rand(1, 120);
        $timestamp = strtotime("-{$days_ago} days") + rand(0, 10);
        $status = rand(0, 7);
        $logs[$timestamp] = [
          'status' => $status,
          'values' => [
            'monitor_task_name' => $task,
            'monitor_type' => $active_monitor,
            'description' => 'Description of the event that occured.',
            'uid' => 1,
            'timestamp' => $timestamp,
            'first_report' => 0,
          ],
        ];
      }
      ksort($logs);
      $ok_status = SystemMonitorLogLevel::STATUS_OK;
      $previous_status = $ok_status;
      foreach ($logs as $log) {
        $log_status = $log_levels[$log['status']];
        if ($log_status > $ok_status && $previous_status == $ok_status) {
          $log['values']['first_report'] = 1;
        }
        $previous_status = $log_status;
        $system_monitor_logger->log($log['status'], 'Test Message', $log['values']);
      }
    }
  }
}
