<?php

namespace Drupal\system_monitor_test\Plugin\SystemMonitorTask;

use Drupal\system_monitor\Plugin\SystemMonitorTask\SystemMonitorTaskBase;

/**
 * A test System Monitor Task.
 *
 * @SystemMonitorTask(
 *   id = "test_system_monitor_task",
 *   label = @Translation("Test System Monitor Task"),
 *   description = @Translation("This is a test task"),
 *   monitor_type = "test",
 *   tags = {
 *     "default",
 *     "test_group"
 *   }
 * )
 */
class TestSystemMonitorTask extends SystemMonitorTaskBase {

  /**
   * The check to run for this System Monitor task.
   *
   * @return int
   *   The status value returned by the monitor task.
   */
  public function runTask() {
    \Drupal::messenger()->addStatus('Success');
    return TRUE;
  }
}
