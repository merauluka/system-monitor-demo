<?php

namespace Drupal\system_monitor_test\Plugin\SystemMonitorTask;

use Drupal\system_monitor\SystemMonitorTaskInterface;

/**
 * A test System Monitor Task.
 *
 * @SystemMonitorTask(
 *   id = "test_system_monitor_task_2",
 *   label = @Translation("Test System Monitor Task 2"),
 *   description = @Translation("This is a test task - 2"),
 *   monitor_type = "test",
 *   tags = {
 *     "default",
 *     "test_group"
 *   }
 * )
 */
class TestSystemMonitorTask2 extends SystemMonitorTaskInterface {

  /**
   * The check to run for this System Monitor task.
   *
   * @return int
   *   The status value returned by the monitor task.
   */
  public function runTask() {
    \Drupal::messenger()->addStatus('Success');
    return TRUE;
  }

}
